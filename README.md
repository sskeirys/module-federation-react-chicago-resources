# Module Federation React Chicago Meetup Resources

This is a list of referenced documentation/demos from the meetup that happened on 27th October, 2021.

**Meetup:** https://www.meetup.com/React-Chicago/events/280454599/

### Resources

- Presentation Video (TBD)
- [Slides (react-chicago-webpack5-mf.pdf)](./react-chicago-webpack5-mf.pdf)
- [PetBook Demo](https://bitbucket.org/sskeirys/petbook-react-chicago/src/master/)
- [Module Federation Examples](https://github.com/module-federation/module-federation-examples)
- [Official Webpack Module Federation Documentation](https://webpack.js.org/concepts/module-federation/)

Thank you for your interest.
